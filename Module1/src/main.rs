#![deny(
    warnings,
    unsafe_code,
    unused_import_braces,
    unused_qualifications,
    trivial_casts,
    trivial_numeric_casts
)]

use clap::{App, Arg, ArgMatches, SubCommand};
use colored::*;
use num_bigint::BigUint;
use sha1::Sha1;
use sha2::Digest;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, Read, Write};
use std::path::PathBuf;
use stringreader::StringReader;

#[cfg(target_pointer_width = "64")]
use blake2::Blake2b as Blake2_512;
#[cfg(target_pointer_width = "32")]
use blake2::Blake2s as Blake2_512;

mod blake2t;
use blake2t::{Blake2_256, Blake2_384};

mod constants;
use constants::*;

fn main() {
    let valid_hashes = [
        SHA3_224,
        SHA3_256,
        SHA3_384,
        SHA3_512,
        SHA2_224,
        SHA2_256,
        SHA2_384,
        SHA2_512,
        SHA2_512_T224,
        SHA2_512_T256,
        BLAKE2_256,
        BLAKE2_384,
        BLAKE2_512,
        WHIRLPOOL,
        SHA1,
        RIPEMD320,
        RIPEMD160,
        RIPEMD128,
        MD5,
    ];
    let create_default_type = vec!["sha3-256", "sha2-256", "sha2-512-t256", "blake2-256"].join(",");
    let matches = App::new("Hashify")
        .version("0.1")
        .author("Michael Lodder")
        .about("Hashify will produce checksums using many different hashes or check that a given input (file or text) matches a given checksum")
        .subcommand(SubCommand::with_name("verify")
            .about("Verify an input matches a digest. If no input is specified or input is '-', input is received from STDIN")
            .arg(Arg::with_name("type")
                 .short("t")
                 .long("type")
                 .value_name("VERIFY_HASH_TYPE")
                 .help("The specific hash to use to compute the checksum.")
                 .takes_value(true)
                 .possible_values(&valid_hashes)
                 .value_delimiter(",")
                 .required(false))
            .arg(Arg::with_name("encoding")
                .short("e")
                .long("encoding")
                .value_name("VERIFY_ENCODING")
                .help("The checksum encoding can only specify one.")
                .takes_value(true)
                .possible_values(&[BLOB, BINARY, BASE10, HEX, BASE58, BASE64, BASE64_URL])
                .max_values(1)
                .required(false))
            .arg(Arg::with_name("byteorder")
                .short("b")
                .long("byteorder")
                .help("The checksum byte order.")
                .value_name("VERIFY_BYTE_ORDER")
                .takes_value(true)
                .possible_values(&["little", "big"])
                .max_values(1)
                .required(false))
            .arg(Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .help("Show all tried algorithms in output.")
                .takes_value(false)
                .required(false))
            .arg(Arg::with_name("CHECKSUM")
                .help("The checksum file or text to compare.")
                .required(true)
                .index(1))
            .arg(Arg::with_name("INPUT")
                .help("The input file or text to check. If no input is specified or input is '-', input is received from STDIN")
                .required(false)
                .index(2)))
        .subcommand(SubCommand::with_name("create")
            .about("Compute the digest checksum for a given input. If no input is specified or input is '-', input is received from STDIN")
            .arg(Arg::with_name("type")
                 .short("t")
                 .long("type")
                 .value_name("OUT_HASH_TYPE")
                 .help("The specific hash to use to compute the checksum")
                 .takes_value(true)
                 .possible_values(&valid_hashes)
                 .value_delimiter(",")
                 .default_value(&create_default_type)
                 .required(false))
            .arg(Arg::with_name("encoding")
                .short("e")
                .long("encoding")
                .value_name("OUT_ENCODING")
                .help("The output encoding.")
                .takes_value(true)
                .possible_values(&[BLOB, BINARY, BASE10, LOWHEX, UPHEX, BASE58, BASE64, BASE64_URL])
                .value_delimiter(",")
                .default_value(LOWHEX)
                .required(false))
            .arg(Arg::with_name("byteorder")
                .short("b")
                .long("byteorder")
                .help("The output byte ordering.")
                .value_name("OUT_BYTE_ORDER")
                .takes_value(true)
                .possible_values(&["little", "big"])
                .value_delimiter(",")
                .default_value("big")
                .required(false))
            .arg(Arg::with_name("INPUT")
                 .help("The file or text to process. If no input is specified or input is '-', input is received from STDIN")
                 .required(false)
                 .index(1))
         ).get_matches();

    if let Some(matches) = matches.subcommand_matches("create") {
        create(matches);
    } else if let Some(matches) = matches.subcommand_matches("verify") {
        verify(matches);
    } else {
        quit("Please specify a command to run [create | verify]".to_string());
    }
}

fn create(matches: &ArgMatches) {
    let hash_types: Vec<&str> = matches.values_of("type").unwrap().collect();
    let out_hash = get_hashes_from_input(&matches, hash_types);
    let label_width = out_hash
        .iter()
        .fold(0usize, |a, (label, _)| ::std::cmp::max(a, label.len()));
    let byte_width = matches
        .values_of("byteorder")
        .unwrap()
        .fold(0usize, |a, s| ::std::cmp::max(a, s.len()));
    let enc_width = matches
        .values_of("encoding")
        .unwrap()
        .fold(0usize, |a, s| ::std::cmp::max(a, s.len()));

    for (label, hash) in out_hash {
        let l = name_color(&label);
        for bo in matches.values_of("byteorder").unwrap() {
            let bytes: Vec<u8> = match bo {
                "big" => hash.to_vec(),
                "little" => {
                    let mut temp = hash.to_vec();
                    temp.reverse();
                    temp
                }
                _ => {
                    quit(format!("Unrecognized byteorder - \"{}\"", bo));
                    Vec::new()
                }
            };

            for enc in matches.values_of("encoding").unwrap() {
                match enc {
                    BLOB => {
                        print!(
                            "{:label_width$} {:byte_width$}-endian {:enc_width$} - {}",
                            l,
                            bo,
                            enc,
                            label_width = label_width,
                            byte_width = byte_width,
                            enc_width = enc_width
                        );
                        io::stdout().write_all(bytes.as_slice()).unwrap();
                        io::stdout().flush().unwrap();
                    }
                    BINARY => println!(
                        "{:label_width$} {:byte_width$}-endian {:enc_width$} - {}",
                        l,
                        bo,
                        enc,
                        bytes
                            .iter()
                            .map(|b| format!("{:b}", b))
                            .collect::<Vec<_>>()
                            .join(""),
                        label_width = label_width,
                        byte_width = byte_width,
                        enc_width = enc_width
                    ),
                    BASE10 => {
                        let n = BigUint::from_bytes_be(hash.as_slice());
                        println!(
                            "{:label_width$} {:byte_width$}-endian {:enc_width$} - {}",
                            l,
                            bo,
                            enc,
                            n.to_str_radix(10),
                            label_width = label_width,
                            byte_width = byte_width,
                            enc_width = enc_width
                        );
                    }
                    LOWHEX => println!(
                        "{:label_width$} {:byte_width$}-endian {:enc_width$} - {}",
                        l,
                        bo,
                        enc,
                        bytes
                            .iter()
                            .map(|b| format!("{:02x}", b))
                            .collect::<Vec<_>>()
                            .join(""),
                        label_width = label_width,
                        byte_width = byte_width,
                        enc_width = enc_width
                    ),
                    UPHEX => println!(
                        "{:label_width$} {:byte_width$}-endian {:enc_width$} - {}",
                        l,
                        bo,
                        enc,
                        bytes
                            .iter()
                            .map(|b| format!("{:02X}", b))
                            .collect::<Vec<_>>()
                            .join(""),
                        label_width = label_width,
                        byte_width = byte_width,
                        enc_width = enc_width
                    ),
                    BASE58 => println!(
                        "{:label_width$} {:byte_width$}-endian {:enc_width$} - {}",
                        l,
                        bo,
                        enc,
                        bs58::encode(bytes.as_slice()).into_string(),
                        label_width = label_width,
                        byte_width = byte_width,
                        enc_width = enc_width
                    ),
                    BASE64 => println!(
                        "{:label_width$} {:byte_width$}-endian {:enc_width$} - {}",
                        l,
                        bo,
                        enc,
                        base64::encode(bytes.as_slice()),
                        label_width = label_width,
                        byte_width = byte_width,
                        enc_width = enc_width
                    ),
                    BASE64_URL => println!(
                        "{:label_width$} {:byte_width$}-endian {:enc_width$} - {}",
                        l,
                        bo,
                        enc,
                        base64_url::encode(bytes.as_slice()),
                        label_width = label_width,
                        byte_width = byte_width,
                        enc_width = enc_width
                    ),
                    _ => println!("Unrecognized encoding - \"{}\"", enc),
                }
            }
        }
    }
}

fn verify(matches: &ArgMatches) {
    let mut checksum = Vec::new();
    let checksum_text = matches.value_of("CHECKSUM").unwrap();
    match get_file(checksum_text) {
        Some(file) => {
            let mut f = File::open(file.as_path())
                .unwrap_or_else(|_| panic!("Unable to read file {}", file.to_str().unwrap()));
            match f.read_to_end(&mut checksum) {
                Ok(_) => (),
                Err(e) => quit(format!(
                    "An error occurred while reading checksum file - \"{}\"",
                    e
                )),
            };
        }
        None => checksum = checksum_text.as_bytes().to_vec(),
    };

    let mut checksum_str = String::new();
    if let Ok(s) = String::from_utf8(checksum.to_vec()).map_err(|e| format!("{}", e)) {
        checksum_str = s.to_string();
    } else {
        quit("Checksum cannot be read properly".to_string());
    }

    let encoding_checksums = get_encoding_checksums(matches, &checksum_str, &checksum);

    let mut hash_types = Vec::new();

    if let Some(ht) = matches.values_of("type") {
        hash_types = ht.collect();
    } else {
        for cksum in encoding_checksums.values() {
            match cksum.len() {
                16 => {
                    hash_types.push(RIPEMD128);
                    hash_types.push(MD5);
                }
                20 => {
                    hash_types.push(SHA1);
                    hash_types.push(RIPEMD160);
                }
                28 => {
                    hash_types.push(SHA2_224);
                    hash_types.push(SHA2_512_T224);
                    hash_types.push(SHA3_224);
                }
                32 => {
                    hash_types.push(SHA2_256);
                    hash_types.push(SHA2_512_T256);
                    hash_types.push(BLAKE2_256);
                    hash_types.push(SHA3_256);
                }
                40 => {
                    hash_types.push(RIPEMD320);
                }
                48 => {
                    hash_types.push(SHA2_384);
                    hash_types.push(SHA3_384);
                    hash_types.push(BLAKE2_384);
                }
                64 => {
                    hash_types.push(SHA2_512);
                    hash_types.push(SHA3_512);
                    hash_types.push(BLAKE2_512);
                    hash_types.push(WHIRLPOOL);
                }
                _ => (),
            }
        }
    }

    if hash_types.is_empty() {
        quit("Unknown checksum length".to_string());
    }

    let mut big_endian = true;
    let mut little_endian = true;
    if let Some(bo) = matches.value_of("byteorder") {
        big_endian = bo == "big";
        little_endian = bo == "little";
    }

    let hashes = get_hashes_from_input(matches, hash_types);

    let mut trials = Vec::new();
    let mut name_width = 0;
    let mut enc_width = 0;
    let mut byte_width = 0;
    for (name, output) in &hashes {
        for (encoding, cksum) in &encoding_checksums {
            if big_endian {
                let l = name_color(name);
                if output == cksum {
                    name_width = ::std::cmp::max(name_width, name.len());
                    enc_width = ::std::cmp::max(enc_width, encoding.len());
                    byte_width = ::std::cmp::max(byte_width, 10);
                    trials.insert(0, (l, "big-endian", encoding, "pass".green()));
                } else {
                    name_width = ::std::cmp::max(name_width, name.len());
                    enc_width = ::std::cmp::max(enc_width, encoding.len());
                    byte_width = ::std::cmp::max(byte_width, 10);
                    trials.push((l, "big-endian", encoding, "fail".red()));
                }
            }
            if little_endian {
                let l = name_color(name);
                let mut temp = output.to_vec();
                temp.reverse();
                if temp == *cksum {
                    name_width = ::std::cmp::max(name_width, name.len());
                    enc_width = ::std::cmp::max(enc_width, encoding.len());
                    byte_width = ::std::cmp::max(byte_width, 13);
                    trials.insert(0, (l, "little-endian", encoding, "pass".green()));
                } else {
                    name_width = ::std::cmp::max(name_width, name.len());
                    enc_width = ::std::cmp::max(enc_width, encoding.len());
                    byte_width = ::std::cmp::max(byte_width, 13);
                    trials.push((l, "little-endian", encoding, "fail".red()));
                }
            }
        }
    }

    match matches.occurrences_of("verbose") {
        1 => {
            for trial in trials {
                println!(
                    "{:name_width$} {:byte_width$} {:enc_width$} - {}",
                    trial.0,
                    trial.1,
                    trial.2,
                    trial.3,
                    name_width = name_width,
                    byte_width = byte_width,
                    enc_width = enc_width
                );
            }
        }
        _ => println!(
            "{:name_width$} {:byte_width$} {:enc_width$} - {}",
            trials[0].0,
            trials[0].1,
            trials[0].2,
            trials[0].3,
            name_width = trials[0].0.len(),
            byte_width = trials[0].1.len(),
            enc_width = trials[0].2.len()
        ),
    };
}

fn name_color(s: &str) -> ColoredString {
    match s {
        MD5 => s.red(),
        SHA1 | RIPEMD320 | RIPEMD160 | RIPEMD128 => s.yellow(),
        _ => s.normal(),
    }
}

fn get_hashes_from_input(matches: &ArgMatches, hash_types: Vec<&str>) -> Vec<(String, Vec<u8>)> {
    match matches.value_of("INPUT") {
        Some(text) => {
            if text == "-" {
                let mut f = io::stdin();
                hash_stream(&mut f, hash_types)
            } else {
                match get_file(text) {
                    Some(file) => {
                        let mut res = Vec::new();
                        match File::open(file.as_path()) {
                            Ok(mut f) => res = hash_stream(&mut f, hash_types),
                            Err(_) => {
                                quit(format!("Unable to read file {}", file.to_str().unwrap()))
                            }
                        };
                        res
                    }
                    None => {
                        let mut f = StringReader::new(text);
                        hash_stream(&mut f, hash_types)
                    }
                }
            }
        }
        None => {
            let mut f = io::stdin();
            hash_stream(&mut f, hash_types)
        }
    }
}

fn get_encoding_checksums(
    matches: &ArgMatches,
    checksum_str: &str,
    checksum: &[u8],
) -> HashMap<String, Vec<u8>> {
    let mut encoding_checksums = HashMap::new();
    if let Some(encoding) = matches.value_of("encoding") {
        match encoding {
            BLOB => {
                encoding_checksums.insert(encoding.to_string(), checksum.to_vec());
            }
            BINARY => {
                if let Some(n) = BigUint::from_radix_be(&checksum_str.as_bytes(), 2) {
                    encoding_checksums.insert(encoding.to_string(), n.to_bytes_be());
                } else {
                    quit(format!("Cannot convert from {}.", encoding));
                }
            }
            BASE10 => {
                if let Some(n) = BigUint::from_radix_be(&checksum_str.as_bytes(), 10) {
                    encoding_checksums.insert(encoding.to_string(), n.to_bytes_be());
                } else {
                    quit(format!("Cannot convert from {}.", encoding));
                }
            }
            HEX => {
                if let Ok(bytes) = hex2bin(&checksum_str) {
                    encoding_checksums.insert(encoding.to_string(), bytes);
                } else {
                    quit(format!("Cannot convert from {}.", encoding));
                }
            }
            BASE58 => {
                if let Ok(bytes) = bs58::decode(&checksum_str).into_vec() {
                    encoding_checksums.insert(encoding.to_string(), bytes);
                } else {
                    quit(format!("Cannot convert from {}.", encoding));
                }
            }
            BASE64 => {
                if let Ok(bytes) = base64::decode(&checksum_str).map_err(|e| format!("{}", e)) {
                    encoding_checksums.insert(encoding.to_string(), bytes);
                } else {
                    quit(format!("Cannot convert from {}.", encoding));
                }
            }
            BASE64_URL => {
                if let Ok(bytes) = base64_url::decode(&checksum_str).map_err(|e| format!("{}", e)) {
                    encoding_checksums.insert(encoding.to_string(), bytes);
                } else {
                    quit(format!("Cannot convert from {}.", encoding));
                }
            }
            enc => {
                quit(format!("Unrecognized encoding - \"{}\"", enc));
            }
        };
    } else {
        //Try to figure out the encoding
        if let Ok(bytes) = base64::decode(&checksum_str).map_err(|e| format!("{}", e)) {
            encoding_checksums.insert(BASE64.to_string(), bytes);
        }
        if let Ok(bytes) = base64_url::decode(&checksum_str).map_err(|e| format!("{}", e)) {
            encoding_checksums.insert(BASE64_URL.to_string(), bytes);
        }
        if let Ok(bytes) = bs58::decode(&checksum_str).into_vec() {
            encoding_checksums.insert(BASE58.to_string(), bytes);
        }
        if let Ok(bytes) = hex2bin(&checksum_str) {
            encoding_checksums.insert(HEX.to_string(), bytes);
        }
        if let Some(n) = BigUint::from_radix_be(&checksum_str.as_bytes(), 10) {
            encoding_checksums.insert(BASE10.to_string(), n.to_bytes_be());
        }
        if let Some(n) = BigUint::from_radix_be(&checksum_str.as_bytes(), 2) {
            encoding_checksums.insert(BINARY.to_string(), n.to_bytes_be());
        }
        if encoding_checksums.is_empty() {
            encoding_checksums.insert(BLOB.to_string(), checksum.to_vec());
        }
    }
    encoding_checksums
}

fn hash_stream<R: Read>(f: &mut R, hash_types: Vec<&str>) -> Vec<(String, Vec<u8>)> {
    let mut out_hash = Vec::new();

    let mut buffer = [0u8; 65536];

    let mut sha3_224 = sha3::Sha3_224::new();
    let mut sha3_256 = sha3::Sha3_256::new();
    let mut sha3_384 = sha3::Sha3_384::new();
    let mut sha3_512 = sha3::Sha3_512::new();
    let mut sha2_224 = sha2::Sha224::new();
    let mut sha2_256 = sha2::Sha256::new();
    let mut sha2_512 = sha2::Sha512::new();
    let mut sha2_384 = sha2::Sha384::new();
    let mut sha2_512t224 = sha2::Sha512Trunc224::new();
    let mut sha2_512t256 = sha2::Sha512Trunc256::new();
    let mut sha1 = Sha1::new();
    let mut blake2_256 = Blake2_256::new();
    let mut blake2_384 = Blake2_384::new();
    let mut blake2_512 = Blake2_512::new();
    let mut whirlpool = whirlpool::Whirlpool::new();
    let mut ripemd320 = ripemd320::Ripemd320::new();
    let mut ripemd160 = ripemd160::Ripemd160::new();
    let mut ripemd128 = ripemd128::Ripemd128::new();
    let mut md5 = md5::Md5::new();

    let mut read = f.read(&mut buffer);
    while read.is_ok() {
        let n = read.unwrap();

        if n == 0 {
            break;
        }

        for hash in &hash_types {
            match *hash {
                SHA3_224 => sha3_224.input(&buffer[..n]),
                SHA3_256 => sha3_256.input(&buffer[..n]),
                SHA3_384 => sha3_384.input(&buffer[..n]),
                SHA3_512 => sha3_512.input(&buffer[..n]),
                SHA2_256 => sha2_256.input(&buffer[..n]),
                SHA2_224 => sha2_224.input(&buffer[..n]),
                SHA2_512_T224 => sha2_512t224.input(&buffer[..n]),
                SHA2_512_T256 => sha2_512t256.input(&buffer[..n]),
                SHA2_384 => sha2_384.input(&buffer[..n]),
                SHA2_512 => sha2_512.input(&buffer[..n]),
                SHA1 => sha1.input(&buffer[..n]),
                BLAKE2_256 => blake2_256.input(&buffer[..n]),
                BLAKE2_384 => blake2_384.input(&buffer[..n]),
                BLAKE2_512 => blake2_512.input(&buffer[..n]),
                WHIRLPOOL => whirlpool.input(&buffer[..n]),
                RIPEMD320 => ripemd320.input(&buffer[..n]),
                RIPEMD160 => ripemd160.input(&buffer[..n]),
                RIPEMD128 => ripemd128.input(&buffer[..n]),
                MD5 => md5.input(&buffer[..n]),
                e => quit(format!("Unrecognized checksum \"{}]\"", e)),
            }
        }

        read = f.read(&mut buffer);
    }
    for hash in hash_types {
        let digest = match hash {
            SHA3_224 => sha3_224.result_reset().as_slice().to_vec(),
            SHA3_256 => sha3_256.result_reset().as_slice().to_vec(),
            SHA3_384 => sha3_384.result_reset().as_slice().to_vec(),
            SHA3_512 => sha3_512.result_reset().as_slice().to_vec(),
            SHA2_256 => sha2_256.result_reset().as_slice().to_vec(),
            SHA2_224 => sha2_224.result_reset().as_slice().to_vec(),
            SHA2_512_T224 => sha2_512t224.result_reset().as_slice().to_vec(),
            SHA2_512_T256 => sha2_512t256.result_reset().as_slice().to_vec(),
            SHA2_384 => sha2_384.result_reset().as_slice().to_vec(),
            SHA2_512 => sha2_512.result_reset().as_slice().to_vec(),
            SHA1 => sha1.result_reset().as_slice().to_vec(),
            BLAKE2_256 => blake2_256.result_reset().as_slice().to_vec(),
            BLAKE2_384 => blake2_384.result_reset().as_slice().to_vec(),
            BLAKE2_512 => blake2_512.result_reset().as_slice().to_vec(),
            WHIRLPOOL => whirlpool.result_reset().as_slice().to_vec(),
            RIPEMD320 => ripemd320.result_reset().as_slice().to_vec(),
            RIPEMD160 => ripemd160.result_reset().as_slice().to_vec(),
            RIPEMD128 => ripemd128.result_reset().as_slice().to_vec(),
            MD5 => md5.result_reset().as_slice().to_vec(),
            e => {
                quit(format!("Unrecognized checksum \"{}]\"", e));
                Vec::new()
            }
        };
        out_hash.push((hash.to_string(), digest))
    }
    out_hash
}

fn get_file(name: &str) -> Option<PathBuf> {
    let mut file = PathBuf::new();
    file.push(name);
    if file.as_path().is_file() {
        let metadata = file
            .as_path()
            .symlink_metadata()
            .expect("symlink_metadata call failed");
        if metadata.file_type().is_symlink() {
            match file.as_path().read_link() {
                Ok(f) => file = f,
                Err(_) => {
                    quit(format!("Can't read the symbolic link: {}", name));
                }
            };
        }
        Some(file)
    } else {
        None
    }
}

fn hex2bin(s: &str) -> Result<Vec<u8>, String> {
    if s.len() % 2 != 0 {
        return Err("Invalid string".to_string());
    }
    for (i, ch) in s.chars().enumerate() {
        if !ch.is_digit(16) {
            return Err(format!("Invalid character position {}", i));
        }
    }

    let input: Vec<_> = s.chars().collect();

    let decoded: Vec<u8> = input
        .chunks(2)
        .map(|chunk| {
            ((chunk[0].to_digit(16).unwrap() << 4) | (chunk[1].to_digit(16).unwrap())) as u8
        })
        .collect();

    Ok(decoded)
}

fn quit(final_message: String) {
    println!("{}", final_message);
    std::process::exit(1);
}

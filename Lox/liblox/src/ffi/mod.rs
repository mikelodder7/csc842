use ffi_support::{FfiStr, ExternError, ErrorCode, ByteBuffer, call_with_result};

use crate::keyring::get_os_keyring;
use crate::KeyRing;

#[no_mangle]
pub extern "C" fn get_secret(service: FfiStr<'_>, id: FfiStr<'_>, err: &mut ExternError) -> ByteBuffer {
    call_with_result(err, || {
        let s = service.as_str();
        let i = id.as_str();

        let mut keyring = get_os_keyring(&s).map_err(|e|e.to_string())?;
        let out = keyring.get_secret(&i).map_err(|e|e.to_string())?;
        Ok(ByteBuffer::from_vec(out.to_vec()))
    })
}

#[no_mangle]
pub extern "C" fn set_secret(service: FfiStr<'_>, id: FfiStr<'_>, secret: *const u8, secret_len: usize, err: &mut ExternError) {
    call_with_result(err, || {
        let s = service.as_str();
        let i = id.as_str();
        if secret.is_null() || secret_len == 0 {
            return Ok(false);
        }
        let r = unsafe { std::slice::from_raw_parts(secret, secret_len) };

        let mut keyring = get_os_keyring(&s).map_err(|e|e.to_string());

        keyring.set_secret(i, r).map_err(|e|e.to_string())
    });
}

#[no_mangle]
pub extern "C" fn delete_secret(service: FfiStr<'_>, id: FfiStr<'_>, err: &mut ExternError) {
    call_with_result(err, || {
        let s = service.as_str();
        let i = id.as_str();

        let mut keyring = get_os_keyring(&s).map_err(|e|e.to_string())?;

        keyring.delete_secret(i).map_err(|e|e.to_string())
    });
}

define_string_destructor!(destroy_string);
define_bytebuffer_destructor!(destroy_bytebuffer);


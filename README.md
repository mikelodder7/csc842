# CSC842

## These are modules that are used for best security practices when developing software.

These are broken into 5 separate modules.

- Module 1: [Hashify](Module1/README.md) is command line tool for verifying integrity and outputting checksums.
- Module 2-4: [Lox](Lox/README.md) is a tool for using an optimal keychain per OS

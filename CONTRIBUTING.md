I love a clean history. I use git rebase to keep the commits in a continual
flow. When contributing, please use `git rebase` before submitting PRs.
#!/usr/bin/perl

use v5.18;
use Cwd;
use File::Spec;
use TOML;

my $config_file = "";
if (@ARGV < 1) {
    $config_file = File::Spec->catfile(Cwd::getcwd() . "config.toml");
    if (! -f $config_file) {
        print STDERR "Cannot find a config file or no parameters were given.";
        exit(1);
    }
} else {

}


sub win32 {
    require "Win32::Process::List";
}

sub darwin {

}

sub linux {
    require "Proc::ProcessTable";
}

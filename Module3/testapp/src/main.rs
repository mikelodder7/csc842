const ENC_KEY: &[u8; 16] = b"1q2w3e4r5t6y7u8i";

use hmac::{Hmac, Mac};
use sha2::Sha256;
use std::io;

fn main() {
    let mut hmac = Hmac::<Sha256>::new_varkey(ENC_KEY).expect("HMAC can take key of any size");
    hmac.input( b"This is a test message");
    let result = hmac.result().code().as_slice().iter()
     .map(|b| format!("{:02x}", b))
     .collect::<Vec<_>>()
     .join("");
    println!("signature = {}", result);
    let prompt = get_input(">>  ");
    println!("{}", prompt);
}

 fn get_input(prompt: &str) -> String{
    println!("{}",prompt);
    let mut input = String::new();
    match io::stdin().read_line(&mut input) {
        Ok(_goes_into_input_above) => {},
        Err(_no_updates_is_fine) => {},
    }
    input.trim().to_string()
}
